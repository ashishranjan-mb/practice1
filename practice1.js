const users = {
    "John": {
        age: 24,
        desgination: "Senior Golang Developer",
        interests: ["Chess, Reading Comics, Playing Video Games"],
        qualification: "Masters",
        nationality: "Greenland"
    },
    "Ron": {
        age: 19,
        desgination: "Intern - Golang",
        interests: ["Video Games"],
        qualification: "Bachelor",
        nationality: "UK"
    },
    "Wanda": {
        age: 24,
        desgination: "Intern - Javascript",
        interests: ["Piano"],
        qualification: "Bachaelor",
        nationality: "Germany"
    },
    "Rob": {
        age: 34,
        desgination: "Senior Javascript Developer",
        interests: ["Walking his dog, Cooking"],
        qualification: "Masters",
        nationality: "USA"
    },
    "Pike": {
        age: 23,
        desgination: "Python Developer",
        interests: ["Listing Songs, Watching Movies"],
        qualification: "Bachaelor's Degree",
        nationality: "Germany"
    }
}


/*

Q1 Find all users who are interested in video games.
Q2 Find all users staying in Germany.
Q3 Sort users based on their seniority level 
   for Designation - Senior Developer > Developer > Intern
   for Age - 20 > 10
Q4 Find all users with masters Degree.
Q5 Group users based on their Programming language mentioned in their designation.


*/
//PROBLEM 1:
let arr = [];
Object.entries(users).find(element => {
    let data = element[1]
     if(data.interests.find(element => element.includes("Video Games"))){
        arr.push(element)
    }
});
console.log(arr)

//PROBLEM 2:
/*let arr1 = [];
Object.entries(users).forEach(element => {
    let data = element[1]
    if(data.nationality.includes("Germany")){
        arr1.push(element)
    }
});
console.log(arr1)
*/

//PROBLEM 3:
/*let arr2 = Object.entries(users);
console.log(arr2.sort(sortFunction));
function sortFunction(a, b){
    if(a[1].desgination === b[1].desgination){
        return (a[1].age > b[1].age) ? -1 : 1
    }
    else{
        return (a[1].desgination > b[1].desgination) ? -1 : 1
    }
}*/

//PROBLEM 4:
/*let arr3 = []
Object.entries(users).forEach(element => {
    let data = element[1]
    if(data.qualification.includes("Masters")){
        arr3.push(element)
    }
});
console.log(arr3)*/

//PROBLEM 5:
/*let res = Object.entries(users).reduce((acc,curr)=>{
    let desgarr = curr[1].desgination
    if(desgarr.includes('Javascript')){
        if(!('Javascript' in acc)){
            acc['Javascript'] = []
        }
        acc['Javascript'].push(curr[0])
    }
    else if(desgarr.includes('Python')){
        if(!('Python' in acc))
            acc['Python'] = []
        acc['Python'].push(curr[0])
    }
    else if(desgarr.includes('Golang')){
        if(!('Golang' in acc))
            acc['Golang'] = []
        acc['Golang'].push(curr[0])
    }
    return acc;
}, {})
console.log(res)*/
